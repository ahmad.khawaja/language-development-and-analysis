package com.language.library;
import java.util.Date;

class Validator {

    // Check if the name begins with a capital letter
    public static boolean isValidName(String name) {
        return name != null && name.matches("^[A-Z].*");
    }

    // Check if the title begins with a capital letter and does not exceed 300 characters
    public static boolean isValidTitle(String title) {
        return title != null && title.matches("^[A-Z].*") && title.length() <= 300;
    }

    // Check if the summary does not exceed 2000 characters
    public static boolean isValidSummary(String summary) {
        return summary != null && summary.length() <= 2000;
    }

    // Check if the email address contains the '@' character
    public static boolean isValidEmail(String email) {
        return email != null && email.contains("@");
    }

    // Check if the date of publication is in the past
    public static boolean isValidDateOfPublication(Date date) {
        return date != null && date.before(new Date());
    }

    // Validate a Customer
    public static boolean isValidCustomer(Customer customer) {
        return isValidName(customer.getName()) && isValidEmail(customer.getEmail());
    }

    // Validate an Item (Article or Book)
    public static boolean isValidItem(Item item) {
        return isValidTitle(item.getTitle()) &&
               item.getAuthors().stream().allMatch(Validator::isValidName) &&
               isValidDateOfPublication(item.getDateOfPublication()) &&
               isValidSummary(item.getSummary());
    }
}

