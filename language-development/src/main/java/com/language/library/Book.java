package com.language.library;
import java.util.List;
import java.util.Date;

class Book extends Item {
    public Book(String title, List<String> authors, Date dateOfPublication, String summary, String content) {
        super(title, authors, dateOfPublication, summary, content);
    }
}
