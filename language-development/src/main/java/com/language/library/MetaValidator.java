package com.language.library;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class MetaValidator {

    // Check if a field name starts with a lower-case letter and does not exceed 20 characters
    public static boolean isValidFieldName(String fieldName) {
        return fieldName != null && fieldName.matches("^[a-z][a-zA-Z0-9]*$") && fieldName.length() <= 20;
    }

    // Check if a method name starts with a lower-case letter and does not exceed 40 characters
    public static boolean isValidMethodName(String methodName) {
        return methodName != null && methodName.matches("^[a-z][a-zA-Z0-9]*$") && methodName.length() <= 40;
    }

    // Validate all fields in a class
    public static boolean validateFields(Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (!isValidFieldName(field.getName())) {
                System.out.println("Invalid field name: " + field.getName() + " in class " + clazz.getName());
                return false;
            }
        }
        return true;
    }

    // Validate all methods in a class, excluding synthetic methods
    public static boolean validateMethods(Class<?> classToValidate) {
        Method[] methods = classToValidate.getDeclaredMethods();
        for (Method method : methods) {
            if (!method.isSynthetic() && !isValidMethodName(method.getName())) {
                System.out.println("Invalid method name: " + method.getName() + " in class " + classToValidate.getName());
                return false;
            }
        }
        return true;
    }

    // Validate a class
    public static boolean validateClass(Class<?> clazz) {
        return validateFields(clazz) && validateMethods(clazz);
    }
}
