package com.language.library;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.ArrayList;

class Library {
    private List<Article> articles;
    private List<Book> books;
    private List<Customer> customers;

    public Library() {
        articles = new ArrayList<>();
        books = new ArrayList<>();
        customers = new ArrayList<>();
    }

    public void addArticle(Article article) {
        articles.add(article);
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

    public void markAsRead(Customer customer, Item item) {
        customer.markAsRead(item);
    }

    public void markAsLiked(Customer customer, Item item) {
        customer.markAsLiked(item);
    }

    public List<Item> listReadItems(Customer customer) {
        return customer.getReadItems();
    }

    public List<Item> listLikedItems(Customer customer) {
        return customer.getLikedItems();
    }

    public List<Item> listMostReadItems() {
        return getMostInteractedItems(Item::getReadCount);
    }

    public List<Item> listMostLikedItems() {
        return getMostInteractedItems(Item::getLikeCount);
    }

    public List<Customer> listCustomersByReadCount() {
        customers.sort((c1, c2) -> Integer.compare(c2.getReadItems().size(), c1.getReadItems().size()));
        return customers;
    }

    private List<Item> getMostInteractedItems(ToIntFunction<Item> countFunction) {
        List<Item> items = new ArrayList<>(articles);
        items.addAll(books);
        items.sort((i1, i2) -> Integer.compare(countFunction.applyAsInt(i2), countFunction.applyAsInt(i1)));
        return items;
    }
}
