package com.language.library;
import java.util.List;
import java.util.Date;

abstract class Item {
    private String title;
    private List<String> authors;
    private Date dateOfPublication;
    private String summary;
    private String content;
    private int readCount;
    private int likeCount;

    public Item(String title, List<String> authors, Date dateOfPublication, String summary, String content) {
        this.title = title;
        this.authors = authors;
        this.dateOfPublication = dateOfPublication;
        this.summary = summary;
        this.content = content;
        this.readCount = 0;
        this.likeCount = 0;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public Date getDateOfPublication() {
        return dateOfPublication;
    }

    public String getSummary() {
        return summary;
    }

    public void incrementReadCount() {
        readCount++;
    }

    public void incrementLikeCount() {
        likeCount++;
    }

    public int getReadCount() {
        return readCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    @Override
    public String toString() {
        return title + " by " + authors;
    }
}
