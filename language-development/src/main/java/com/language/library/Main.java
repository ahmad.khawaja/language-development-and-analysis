package com.language.library;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.Duration;

//Example usage
public class Main {

	public static void main(String[] args) throws Exception {
        validatorRunner();
        metaValidatorRunner();
    }
	
	public static void validatorRunner() throws Exception {
		// Get the current date and time
        LocalDateTime now = LocalDateTime.now();
        
        // Add 10 minutes to the current date and time
        LocalDateTime later = now.plus(Duration.ofMinutes(10));
        
        // Convert LocalDateTime to Date
        ZonedDateTime zonedDateTime = later.atZone(ZoneId.systemDefault());
        Date date = Date.from(zonedDateTime.toInstant());
        
        // Sample date formatter
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	     
	     // Sample Articles and Books
	     Article article1 = new Article("Java Programming", Arrays.asList("John Doe"), sdf.parse("2020-01-01"), "A comprehensive guide to Java.", "Content of the article.");
	     Article article2 = new Article("java Programming", Arrays.asList("John Doe"), sdf.parse("2020-01-01"), "A comprehensive guide to Java.", "Content of the article.");
	     Article article3 = new Article("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id mi mauris. Vestibulum malesuada nulla ac tempus scelerisque. Nulla sodales velit lorem, id semper eros suscipit ac. Nunc ut laoreet augue. Sed rhoncus at elit eget lacinia. Nunc posuere rutrum ante non tristique. Pellentesque vitae lacus ut neque suscipit molestie at quis eros.", Arrays.asList("John Doe"), sdf.parse("2020-01-01"), "A comprehensive guide to Java.", "Content of the article.");
	     Book book1 = new Book("Learning Python", Arrays.asList("Jane Smith"), sdf.parse("2019-05-15"), "An in-depth look at Python programming.", "Content of the book.");
	     Book book2 = new Book("Learning Python", Arrays.asList("Jane Smith"), sdf.parse("2019-05-15"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id mi mauris. Vestibulum malesuada nulla ac tempus scelerisque. Nulla sodales velit lorem, id semper eros suscipit ac. Nunc ut laoreet augue. Sed rhoncus at elit eget lacinia. Nunc posuere rutrum ante non tristique. Pellentesque vitae lacus ut neque suscipit molestie at quis eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id mi mauris. Vestibulum malesuada nulla ac tempus scelerisque. Nulla sodales velit lorem, id semper eros suscipit ac. Nunc ut laoreet augue. Sed rhoncus at elit eget lacinia. Nunc posuere rutrum ante non tristique. Pellentesque vitae lacus ut neque suscipit molestie at quis eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id mi mauris. Vestibulum malesuada nulla ac tempus scelerisque. Nulla sodales velit lorem, id semper eros suscipit ac. Nunc ut laoreet augue. Sed rhoncus at elit eget lacinia. Nunc posuere rutrum ante non tristique. Pellentesque vitae lacus ut neque suscipit molestie at quis eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id mi mauris. Vestibulum malesuada nulla ac tempus scelerisque. Nulla sodales velit lorem, id semper eros suscipit ac. Nunc ut laoreet augue. Sed rhoncus at elit eget lacinia. Nunc posuere rutrum ante non tristique. Pellentesque vitae lacus ut neque suscipit molestie at quis eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id mi mauris. Vestibulum malesuada nulla ac tempus scelerisque. Nulla sodales velit lorem, id semper eros suscipit ac. Nunc ut laoreet augue. Sed rhoncus at elit eget lacinia. Nunc posuere rutrum ante non tristique. Pellentesque vitae lacus ut neque suscipit molestie at quis eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id mi mauris. Vestibulum malesuada nulla ac tempus scelerisque. Nulla sodales velit lorem, id semper eros suscipit ac. Nunc ut laoreet augue. Sed rhoncus at elit eget lacinia. Nunc posuere rutrum ante non tristique. Pellentesque vitae lacus ut neque suscipit molestie at quis eros.", "Content of the book.");
	     Book book3 = new Book("Learning Python", Arrays.asList("Jane Smith"), date, "An in-depth look at Python programming.", "Content of the book.");
	     
	     
	     // Sample Customers
	     Customer customer1 = new Customer("Alice Johnson", "123 Main St", "alice@example.com");
	     Customer customer2 = new Customer("alice Johnson", "123 Main St", "alice@example.com");
	     Customer customer3 = new Customer("Alice Johnson", "123 Main St", "aliceexample.com");
	     
	     // Validate Customers and Items
	     System.out.println("Validating Customers and Items:");
	     System.out.println("Is customer1 valid? " + Validator.isValidCustomer(customer1));
	     System.out.println("Is customer2 valid? " + Validator.isValidCustomer(customer2));
	     System.out.println("Is customer3 valid? " + Validator.isValidCustomer(customer3));
	     System.out.println("Is article1 valid? " + Validator.isValidItem(article1));
	     System.out.println("Is article2 valid? " + Validator.isValidItem(article2));
	     System.out.println("Is article3 valid? " + Validator.isValidItem(article3));
	     System.out.println("Is book1 valid? " + Validator.isValidItem(book1));
	     System.out.println("Is book2 valid? " + Validator.isValidItem(book2));
	     System.out.println("Is book3 valid? " + Validator.isValidItem(book3));
	}
	
	public static void metaValidatorRunner() {
		System.out.println("Validating SampleClass:");
        boolean isClassValid1 = MetaValidator.validateClass(SampleClass1.class);
        boolean isClassValid2 = MetaValidator.validateClass(SampleClass2.class);
        boolean isClassValid3 = MetaValidator.validateClass(SampleClass3.class);
        boolean isClassValid4 = MetaValidator.validateClass(SampleClass4.class);
        boolean isClassValid5 = MetaValidator.validateClass(SampleClass5.class);
        boolean isClassValid6 = MetaValidator.validateClass(Library.class);
        
        System.out.println("Is SampleClass1 valid? " + isClassValid1);
        System.out.println("Is SampleClass2 valid? " + isClassValid2);
        System.out.println("Is SampleClass3 valid? " + isClassValid3);
        System.out.println("Is SampleClass4 valid? " + isClassValid4);
        System.out.println("Is SampleClass5 valid? " + isClassValid5);
        System.out.println("Is LibraryClass valid? " + isClassValid6);
	}
}
