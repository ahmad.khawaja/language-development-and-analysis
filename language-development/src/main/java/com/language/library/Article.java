package com.language.library;
import java.util.List;
import java.util.Date;

class Article extends Item {
    public Article(String title, List<String> authors, Date dateOfPublication, String summary, String content) {
        super(title, authors, dateOfPublication, summary, content);
    }
}