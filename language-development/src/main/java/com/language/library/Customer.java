package com.language.library;
import java.util.List;
import java.util.ArrayList;

class Customer {
    private String name;
    private String address;
    private String email;
    private List<Item> readItems;
    private List<Item> likedItems;

    public Customer(String name, String address, String email) {
        this.name = name;
        this.address = address;
        this.email = email;
        this.readItems = new ArrayList<>();
        this.likedItems = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void markAsRead(Item item) {
        if (!readItems.contains(item)) {
            readItems.add(item);
            item.incrementReadCount();
        }
    }

    public void markAsLiked(Item item) {
        if (!likedItems.contains(item)) {
            likedItems.add(item);
            item.incrementLikeCount();
        }
    }

    public List<Item> getReadItems() {
        return readItems;
    }

    public List<Item> getLikedItems() {
        return likedItems;
    }

    @Override
    public String toString() {
        return name + " (" + email + ")";
    }
}
